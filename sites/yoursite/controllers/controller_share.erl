-module(controller_share).

-export([
    html/1
]).

-include("include/controller_html_helper.hrl").

html(Context) ->
    {<<"Hello world and all the people in it!">>, Context}.
