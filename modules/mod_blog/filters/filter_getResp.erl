-module(filter_getResp).
-export([getResp/2]).


getResp(Data, _Context) ->
ssl:start(),
application:start(inets),
case httpc:request(get, {Data, []}, [], []) of
        {ok,{{_,Status,Detail},_,_}} -> {Status, Detail};
        _ -> {error, "something went wrong"}
end.
