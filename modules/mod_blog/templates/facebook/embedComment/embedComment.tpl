<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.11';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="fb-comments" 
data-href="{% block url %}default{% endblock %}" 
data-numposts="{% block comment_num %}5{% endblock %}" 
data-mobile="{% block plugin_responsive %}auto-detected{% endblock %}"
data-order-by="{% block comment_order %}reverse_time{% endblock %}"
data-width="{% block plugin_width %}550{% endblock %}"></div>

<script>
    var x = document.getElementsByClassName("fb-comments")[0].getAttribute("data-href"); 
    if(x == "default"){
    	x = window.location.href;
    	document.getElementsByClassName("fb-comments")[0].setAttribute("data-href", x);
    }
    console.log(x);
</script>