<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.11';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="fb-page"
    data-href="{% block data %}https://www.facebook.com/facebook{% endblock %}"
    data-tabs="{% block tabs %}timeline{% endblock %}"
    data-small-header="{% block smallHeader %}false{% endblock %}"
    data-adapt-container-width="{% block containerWidth %}true{% endblock %}"
    data-hide-cover="{% block hideCover %}false{% endblock %}"
    data-show-facepile="{% block showFacepile %}true{% endblock %}"
>
</div>
