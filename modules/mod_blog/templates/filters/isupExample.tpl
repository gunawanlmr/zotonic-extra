{% if "http://scele.cs.ui.ac.id"|isup %}
  <button class="btn btn-sm btn-success"> http://scele.cs.ui.ac.id is available </button>
{% else %}
  <button class="btn btn-sm btn-danger"> http://scele.cs.ui.ac.id is available </button>
{% endif %}
{% if "https://scele.cs.ui.ac.id/scele_2015/asldkfj"|isup %}
  <button class="btn btn-sm btn-success"> https://scele.cs.ui.ac.id/scele_2015/asldkfj is available </button>
{% else %}
  <button class="btn btn-sm btn-danger"> https://scele.cs.ui.ac.id/scele_2015/asldkfj is not available </button>
{% endif %}
